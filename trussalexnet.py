import torch
import torch.nn as nn
import torchvision.datasets as datasets
from torchvision.transforms import transforms
from torch.utils.data import DataLoader
from torch.optim import Adam
from torch.autograd import Variable
import numpy as np


# unit of conv->max pool->relu to make structure a little cleaner
class Unit(nn.Module):
    def __init__(self, in_channels, out_channels, filter_size, n_stride, n_padding):
        super(Unit, self).__init__()

        self.conv = nn.Conv2d(in_channels=in_channels, kernel_size=filter_size, out_channels=out_channels, stride=n_stride, padding=n_padding)
        self.mp = nn.MaxPool2d(kernel_size=3, stride=2)
        self.relu = nn.ReLU()

    def forward(self, input):
        output = self.conv(input)
        output = self.relu(output)
        output = self.mp(output)

        return output


# building net with 6 classes
class TrussAlexNet(nn.Module):
    def __init__(self, num_classes=6):
        super(TrussAlexNet, self).__init__()

        self.unit1 = Unit(in_channels=3, out_channels=48, filter_size=11, n_stride=4, n_padding=0)
        self.unit2 = Unit(in_channels=48, out_channels=128, filter_size=5, n_stride=1, n_padding=2)
        self.unit3 = nn.Conv2d(in_channels=128, out_channels=192, kernel_size=3, stride=1, padding=1)
        self.unit3relu = nn.ReLU()
        self.unit4 = nn.Conv2d(in_channels=192, out_channels=192, kernel_size=3, stride=1, padding=1)
        self.unit4relu = nn.ReLU()
        self.unit5 = Unit(in_channels=192, out_channels=128, filter_size=3, n_stride=1, n_padding=1)

        # Add all the units into the Sequential layer in exact order
        self.net = nn.Sequential(self.unit1, self.unit2, self.unit3, self.unit3relu, self.unit4, self.unit4relu, self.unit5)

        self.fc1 = nn.Linear(in_features=4608, out_features=2048)
        self.fc2 = nn.Linear(in_features=2048, out_features=2048)
        self.fc3 = nn.Linear(in_features=2048, out_features=num_classes)
        self.dropout = nn.Dropout(0.5)

    def forward(self, input):
        output = self.net(input)
        #print(output.shape)
        # output = output.view(-1, 128)
        output = output.reshape(output.size(0), -1)
        #print(output.shape)
        output = self.dropout(self.fc1(output))
        output = self.dropout(self.fc2(output))
        output = self.fc3(output)
        #print(output.shape)
        return output


# Define transformations for the training set, set size to 227*227 and convert to tensor and normalize
train_transformations = transforms.Compose([
    #transforms.RandomHorizontalFlip(),
    #transforms.RandomCrop(32, padding=4),
    transforms.Resize((227, 227)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

batch_size = 32

# Load the training set
train_set = datasets.ImageFolder(root='./datareduced', transform=train_transformations)

# Create a loader for the training set
train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=0)

print("Training set loaded")

# Define transformations for the test set
test_transformations = transforms.Compose([
    transforms.Resize((227, 227)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Load the test set, note that train is set to False
test_set = datasets.ImageFolder(root="./testincreased", transform=test_transformations)

# Create a loder for the test set, note that both shuffle is set to false for the test loader
test_loader = DataLoader(test_set, batch_size=batch_size, shuffle=True, num_workers=0)

print("Test set loaded")

# Check if gpu support is available
cuda_avail = torch.cuda.is_available()

# Create model, optimizer and loss function
model = TrussAlexNet(num_classes=6)

if cuda_avail:
    model.cuda()

optimizer = Adam(model.parameters(), lr=0.0001, weight_decay=0.0001) #divided lr by 10
loss_fn = nn.CrossEntropyLoss()
#loss_fn = nn.NLLLoss() #negative likelihood loss as described in paper


# Create a learning rate adjustment function that divides the learning rate by 10 every 30 epochs
def adjust_learning_rate(epoch):
    lr = 0.0001
#    lr = 0.01 #matches original Alexnet LR

    if epoch > 180:
        lr = lr / 1000000
    elif epoch > 150:
        lr = lr / 100000
    elif epoch > 120:
        lr = lr / 10000
    elif epoch > 90:
        lr = lr / 1000
    elif epoch > 60:
        lr = lr / 100
    elif epoch > 30:
        lr = lr / 10

    for param_group in optimizer.param_groups:
        param_group["lr"] = lr


def save_models(epoch, acc):
    torch.save(model.state_dict(), "TANmodel_{}_{}.model".format(epoch, acc))
    print("Checkpoint saved")


def test():
    model.eval()
    test_acc = 0.0
    for i, (images, labels) in enumerate(test_loader):

        if cuda_avail:
            images = Variable(images.cuda())
            labels = Variable(labels.cuda())

        # Predict classes using images from the test set
        outputs = model(images)
        _, prediction = torch.max(outputs.data, 1)
        prediction = prediction.cpu().numpy()
        test_acc += torch.sum(prediction == labels.data).float()

    # Compute the average acc and loss over all 72 test images
    test_acc = test_acc / 186

    return test_acc


def train(num_epochs):
    best_acc = 0.0

    for epoch in range(num_epochs):
        model.train()
        train_acc = 0.0
        train_loss = 0.0
        batchnum = 0
        for i, (images, labels) in enumerate(train_loader):
            # Move images and labels to gpu if available
            if cuda_avail:
                images = Variable(images.cuda())
                labels = Variable(labels.cuda())

            # Clear all accumulated gradients
            optimizer.zero_grad()
            # Predict classes using images from the test set
            outputs = model(images)
            #print(outputs.shape)
            # Compute the loss based on the predictions and actual labels
            loss = loss_fn(outputs, labels)
            f = open("TANoutput.txt", "w")
            f.write("Epoch {}, Batch {}".format(epoch, batchnum))
            f.close()
            print("Epoch {}, Batch {}".format(epoch, batchnum))
            # Backpropagate the loss
            loss.backward()

            # Adjust parameters according to the computed gradients
            optimizer.step()

            #train_loss += loss.cpu().data[0] * images.size(0)
            train_loss += loss.cpu().data * images.size(0)
            _, prediction = torch.max(outputs.data, 1)

            train_acc += torch.sum(prediction == labels.data).float()
#            print(train_acc)

            batchnum += 1
        # Call the learning rate adjustment function
        adjust_learning_rate(epoch)

        # Compute the average acc and loss over all 25528 training images
        train_acc = train_acc / 1806
        train_loss = train_loss / 1806

        # Evaluate on the test set
        test_acc = test()

        # Save the model if the test acc is greater than our current best
        if test_acc > best_acc:
            save_models(epoch, test_acc)
            best_acc = test_acc
        f2 = open("TANaccuracy.txt", "a")
        f2.write("Epoch {}, Train Accuracy: {} , TrainLoss: {} , Test Accuracy: {}\n".format(epoch, train_acc, train_loss, test_acc))
        f2.close()

        # Print the metrics
        print("Epoch {}, Train Accuracy: {} , TrainLoss: {} , Test Accuracy: {}".format(epoch, train_acc, train_loss,
                                                                                        test_acc))


if __name__ == "__main__":
    train(200)
