import glob
import os
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def sorttoLabel(labelfile, storepath):
    label_list = []
    lvector_list = []
    # create directories to sort data into
    with open(labelfile, 'r') as labelmaker:
        lines = [line.rstrip('\n') for line in labelmaker]  # strip newline
        for label in lines:
            label_list.append(label)
            # create label directory if necessary
            if not os.path.exists(storepath + label):
                os.mkdir(storepath + label)
            # add to label array as a vector
            lvector = label[1:-1].split(',')
            lvector_list.append([float(lvector[0]), float(lvector[1]), float(lvector[2])])

    # create list of files in data directory
    path = 'C:/Users/susan/Documents/School/ASU Semester 6/CSE493/Image Simulation/data2/data2/*.jpg'
    files = glob.glob(path)

    for filepath in files:
        filename = filepath.split('\\')[1] #depends on how many * in filepath - UPDATE WHEN PATH CHANGE
        gaze = filename.split(' ', 1)[0][1:-1]
        vector = gaze.split(',')
        ivector = [float(vector[0]), float(vector[1]), float(vector[2])]

        # sort image vector to closest pose label
        ilabel = ""
        minAngDiff = math.inf
        for i in range(0, len(lvector_list)):
            # calculate angle between pose label and image vector
            angDiff = np.arccos(np.clip(np.dot(ivector, lvector_list[i]) / (np.linalg.norm(ivector) * np.linalg.norm(lvector_list[i])), -1.0, 1.0))
            if angDiff < minAngDiff:
                minAngDiff = angDiff
                ilabel = label_list[i]

        print("{} moving to {}".format(ivector, ilabel))
        os.rename(filepath, storepath + ilabel + "/" + filename)



def graphdata():
    # create list of files in data directory
    path = 'C:/Users/susan/Documents/School/ASU Semester 6/CSE493/Image Simulation/data2/data2/*.jpg'
    files = glob.glob(path)

    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #i = 1
    seen = set() # avoid duplicates
    for filepath in files:
        filename = filepath.split('\\')[1] #depends on how many * in filepath - UPDATE WHEN PATH CHANGE
        gaze = filename.split(' ', 1)[0][1:-1]
        vector = gaze.split(',')
        if gaze not in seen:
            seen.add(gaze)
            print(gaze)
            ax.scatter(float(vector[0]), float(vector[1]), float(vector[2]), c='k', marker='o')
            #i += 1
        #if i % 50 == 0:
            #plt.pause(1)
    plt.show()


def poselabelgen(num_labels, radius): # note that n might not equal num_labels, so aim high
    labels = []
    count = 0  # tracks number of labels created
    fig = plt.figure()  # plotting
    ax = fig.add_subplot(111, projection='3d')
    a = (4 * math.pi)/float(num_labels) # surface area allotted to each label
    d = math.sqrt(a) # approximate separation between labels
    M_phi = round(math.pi / d)
    d_phi = math.pi / M_phi
    d_theta = a / d_phi
    for m in range(0, M_phi):
        phi = math.pi * (m + 0.5) / M_phi
        M_theta = round(2 * math.pi * math.sin(phi) / d_theta)
        for n in range(0, M_theta):
            theta = (2 * math.pi * n) / M_theta
            x = radius * math.sin(phi) * math.cos(theta)
            y = radius * math.sin(phi) * math.sin(theta)
            z = radius * math.cos(phi)
            labels.append("[{},{},{}]".format(x, y, z))
            ax.scatter(x, y, z, c='k', marker='o')
            count += 1
    print("{} pose labels created".format(count))
    for label in labels:
        print(label)
    #with open("labels_{}_{}".format(radius, num_labels), mode='w') as labelfile:
    #    for label in labels:
    #        labelfile.write(label + "\n")
    #    labelfile.close()
    plt.show()

if __name__ == "__main__":
    # graphdata()
    poselabelgen(162, 2)
    #sorttoLabel("labels_2_162.txt", "C:/Users/susan/Documents/School/ASU Semester 6/CSE493/Image Simulation/data2/data2/")
    #print('works')