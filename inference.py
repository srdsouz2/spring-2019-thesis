import sys
import json
from PIL import Image
import torch
from torchvision.transforms import transforms
from torch.autograd import Variable
from torch.utils.data import DataLoader
from INalexnet import INAlexNet

model_path = 'INANmodel_3_0.7930900454521179.model'
num_classes = 162
labeldict = {}

def inference_INAN(test_image):
    model = INAlexNet(num_classes)
    model.load_state_dict(torch.load(model_path))
    model.eval()
    output = model(test_image)
    # print(output.data)
    _, prediction = torch.max(output.data, 1)
    prediction = prediction.cpu().numpy()
    # print(prediction)
    print(labeldict[prediction[0]])


if __name__ == '__main__':
    # parse command line argument to get input name
    # filepath = sys.argv[1]
    # print pose label name
    # pose_label = filepath.split('/')[-2]
    # print("Actual pose label is {}".format(pose_label))
    filepath = 'C:/Users/susan/Documents/School/ASU Semester 6/CSE493/Image Simulation/data2_162/data2/' + \
               '[0.404726191475888,-1.2456191365689808,-1.5114991487085163]/' + \
               '[0.275899,-1.38704,-1.41421] [-0.981877,-0.00208632,-0.189508]_0_0.jpg'
    # import label to class mapping used in model training and invert
    with open('labeldict.json', 'r') as labelfile:
        labeldict = json.load(labelfile)
    labeldict = {v:k for k,v in labeldict.items()}
    # process image for input to model
    test_image = Image.open(filepath)
    test_transformations = transforms.Compose([
        transforms.Resize((227, 227)),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])
    test_image = test_transformations(test_image).float()
    test_image = Variable(test_image, requires_grad=True)
    test_image = test_image.unsqueeze(0) # adds batch dimension
    cuda_avail = torch.cuda.is_available()
    if cuda_avail:
        test_image = test_image.cuda()
    # pass to inference function which loads model and applies inference
    inference_INAN(test_image)