import torch
import torch.nn as nn
import torchvision.datasets as datasets
import torchvision.models as models
from torchvision.transforms import transforms
from torch.utils.data import DataLoader
from torch.optim import Adam
from torch.autograd import Variable
import numpy as np
import json

batch_size = 16
classes = 162
test_num = 5152
train_num = 9108
load_model = True
model_name = 'INAN162_.00005/INANmodel_24_0.7998835444450378.model'

# modifying pretrained AlexNet to have x classes
class INAlexNet(nn.Module):
    def __init__(self, num_classes):
        super(INAlexNet, self).__init__()
        self.alexnet = models.alexnet(pretrained=True)

        #set all parameters to false so they won't automatically train
        for param in self.alexnet.parameters():
            param.requires_grad = False
            #print("falsified")

        #reconfigure last three layers - will automatically have requires_grad = True
        self.alexnet.classifier[1] = nn.Linear(in_features=9216, out_features=4096)
        self.alexnet.classifier[4] = nn.Linear(in_features=4096, out_features=4096)
        self.alexnet.classifier[6] = nn.Linear(in_features=4096, out_features=num_classes)
        #print(alexnet)


    def forward(self, input):
        output = self.alexnet(input)
        return output


# Define transformations for the training set, set size to 227*227 and convert to tensor and normalize
train_transformations = transforms.Compose([
    transforms.Resize((227, 227)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Load the training set
train_set = datasets.ImageFolder(root='./data2_162_reduced', transform=train_transformations)
with open('labeldict.json', 'w') as labelfile:
    json.dump(train_set.class_to_idx, labelfile)
print("Label Mapping saved to labeldict.json")

# Create a loader for the training set
train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=0)

print("Training set loaded")

# Define transformations for the test set, set size to 227*227 and convert to tensor and normalize
test_transformations = transforms.Compose([
    transforms.Resize((227, 227)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Load the test set, note that train is set to False
test_set = datasets.ImageFolder(root="./data2_162_test", transform=test_transformations)

# Create a loader for the test set, note that both shuffle is set to false for the test loader
test_loader = DataLoader(test_set, batch_size=batch_size, shuffle=False, num_workers=0)

print("Test set loaded")

# Check if gpu support is available
cuda_avail = torch.cuda.is_available()

# Create model, optimizer and loss function
model = INAlexNet(num_classes=classes)

if load_model:
    model.load_state_dict(torch.load(model_name))

if cuda_avail:
    model.cuda()

# collect parameters to optimize
print("Optimizing:")
params_to_update = []
for name,param in model.named_parameters():
    if param.requires_grad:
        params_to_update.append(param)
        print("\t", name)

optimizer = Adam(params_to_update, lr=0.00005, weight_decay=0.0001)
loss_fn = nn.CrossEntropyLoss()

# Create a learning rate adjustment function to modify as needed
def adjust_learning_rate(epoch):
    lr = 0.00005

    if epoch > 100:
        lr = 0.00001
    if epoch > 50:
        lr = 0.00005

    for param_group in optimizer.param_groups:
        param_group["lr"] = lr

def save_models(epoch, acc):
    torch.save(model.state_dict(), "INANmodel_{}_{}.model".format(epoch, acc))
    print("Checkpoint saved")

def test():
    model.eval()
    test_acc = 0.0
    for i, (images, labels) in enumerate(test_loader):

        if cuda_avail:
            images = Variable(images.cuda())
            labels = Variable(labels.cuda())

        # Predict classes using images from the test set
        outputs = model(images)
        _, prediction = torch.max(outputs.data, 1)
        prediction = prediction.cpu().numpy()
        test_acc += torch.sum(prediction == labels.data).float()

    # Compute the average acc and loss over all 186 test images
    test_acc = test_acc / test_num

    return test_acc

def train(num_epochs):
    best_acc = 0.0

    for epoch in range(num_epochs):
        model.train()
        train_acc = 0.0
        train_loss = 0.0
        batchnum = 0
        for i, (images, labels) in enumerate(train_loader):
            # Move images and labels to gpu if available
            if cuda_avail:
                images = Variable(images.cuda())
                labels = Variable(labels.cuda())

            # Clear all accumulated gradients
            optimizer.zero_grad()
            # Predict classes using images from the test set
            outputs = model(images)
            # Compute the loss based on the predictions and actual labels
            loss = loss_fn(outputs, labels)
            # Write progress to file and stdout
            f = open("INANoutput.txt", "w")
            f.write("Epoch {}, Batch {}".format(epoch, batchnum))
            f.close()
            print("Epoch {}, Batch {}".format(epoch, batchnum))
            # Backpropagate the loss
            loss.backward()

            # Adjust parameters according to the computed gradients
            optimizer.step()

            train_loss += loss.cpu().data * images.size(0)
            _, prediction = torch.max(outputs.data, 1)

            train_acc += torch.sum(prediction == labels.data).float()

            batchnum += 1
        # Call the learning rate adjustment function
        adjust_learning_rate(epoch)

        # Compute the average acc and loss over all 1806 training images
        train_acc = train_acc / train_num
        train_loss = train_loss / train_num

        # Evaluate on the test set
        test_acc = test()

        # Save the model if the test acc is greater than our current best
        if test_acc > best_acc:
            save_models(epoch, test_acc)
            best_acc = test_acc
        # Write progress to file and stdout
        f2 = open("INANaccuracy.txt", "a")
        f2.write("Epoch {}, Train Accuracy: {} , TrainLoss: {} , Test Accuracy: {}\n".format(epoch, train_acc, train_loss, test_acc))
        f2.close()
        print("Epoch {}, Train Accuracy: {} , TrainLoss: {} , Test Accuracy: {}".format(epoch, train_acc, train_loss, test_acc))


if __name__ == "__main__":
   # train(200)
   print('testing')

