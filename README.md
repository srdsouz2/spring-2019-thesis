# Spring 2019 Thesis

Contains various code used to complete Spring 2019 Honors Thesis.  
-sortdata.py: graph data, generate and graph pose labels, sort data to pose labels  
-inference.py: feed in file name and classify with specified model    
-trussalexnet.py: trains modified AlexNet model from scratch on synthetic dataset  
-INalexnet.py: transfer learning with pytorch AlexNet pretrained on ImageNet to work with synthetic dataset  